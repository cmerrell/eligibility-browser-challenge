# Frontend Software Development Engineer -- Technical Coding Problem

The  goal of  this coding  exercise is  to give  us insight  into your
ability to  write code that solves  a problem for a  company.  It will 
help us evaluate  your ability to assess  requirements  and  produce a
design that reacts to those requirements.

## Problem Statement

The Glorious Gumball Co. has asked for help with visualizing their
medical claim data for their employees, retirees and dependents.

The intent is to create an "eligibility browser" single-page web
interface to find eligible employees, dependents, and retirees, and
show their employment history alongside the time and amount for each
claim.

There is an API which goes to fetch all the data in one JSON blob.
You can see an example of this data in the `api-data.json` file.

Build a single-page application that displays this data in a similar 
way to that exemplified in the `eligibility-browser.jpg` diagram.  Note 
that you will need to calculate some information in order to display 
the data effectively, including:

* current status of each person, one of:
  * empl: current employee
  * empl, termed: former employee, no retirement plan
  * dep: current dependent
  * dep, termed: former dependent
  * ret: current retiree
  * ret, termed: former retiree, termed out of eligibility
* number of valid claims for each person (displayed in title of
    "claims" column)
* total claim amount for each person


## Criteria for Evaluation
The applet you submit should show an understanding of the following:
* Typescript
* State Management (store, composables/hooks, provide/inject, prop-drilling).
  * Not all of the above strategies are required or expected.
  * Your output should contain more than just prop-drilling.
* Unit tests
  * Write one unit test that:
    * Runs successfully
    * Meaningfully verifies an expected user interaction (e.g. click).
  * Additional coverage is not required or expected.


### SPA Framework

Submissions must use one of the following SPA frameworks:
* Vue (either v2/v3)
* React

We will not evaluate on your choice of framework. Choose the one you 
are most confident in.


## Further Questions

#### Describe strategies for scaling the above application for the following cases:
* 100 claims per year for a person
* 10,000 employees, and more than 10,000 dependents

#### Describe the backend's role in facilitating the above.
* What API endpoint(s) would you write, and how would you structure them?
* Would you recommend a specific data storage solution? Why?


## Submission Format

Please package your project as a `.zip` file that contains:
- A root-level `README.md` file with instructions on how to: 
  - Install package dependencies.
  - Run the SPA application.
  - Any other steps needed to deploy a functional instance of your submission.
- Any notes about your implementation that will make it easier for us to evaluate your work.
- Additional responses to the 'Further Questions' section above.